Template.login.onCreated(function() {

});

Template.login.onRendered(function() {
    
});

Template.login.helpers({
    areFilled: function() {
        return Session.get("filledFields");
    }
});

Template.login.events({
    'click #logmein' (event) {
        event.preventDefault();
        console.log("clicked login");
        let email = $("#email").val();
        let pass = $("#password").val();

        if (email == null || email == "" || pass == "" || pass == null) {
            Session.set("filledFields", false);
            return;
        } else {
            Meteor.loginWithPassword(email, pass);
        }
    },
    'click #reg' (event) {
        event.preventDefault();
        FlowRouter.go('/reg');
    },
});