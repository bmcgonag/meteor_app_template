FlowRouter.route('/dashboard', {
    name: 'home',
    action() {
        BlazeLayout.render('MainLayout', { main: "dashboard" });
    }
});

FlowRouter.route('/', {
    name: 'homeNoRoute',
    action() {
        BlazeLayout.render('MainLayout', { main: "dashboard" });
    }
});

FlowRouter.route('/', {
    name: 'homeNotLoggedIn',
    action() {
        BlazeLayout.render('MainLayout', { notLoggedIn: "login" });
    }
});

FlowRouter.route('/login', {
    name: 'login',
    action() {
        BlazeLayout.render('MainLayout', { notLoggedIn: "login" });
    }
});

FlowRouter.route('/reg', {
    name: 'reg',
    action() {
        BlazeLayout.render('MainLayout', { notLoggedIn: "reg" });
    }
});

FlowRouter.route('/userMgmt', {
    name: 'userMgmt',
    action() {
        BlazeLayout.render('MainLayout', { main: 'userMgmt' });
    }
});
