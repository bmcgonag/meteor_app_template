import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  // code to run on server at startup
  Roles.createRole("game-master", {unlessExists: true});
  Roles.createRole("team-captain", {unlessExists: true});
  Roles.createRole("team-member", {unlessExists: true});
  Roles.createRole("systemadmin", {unlessExists: true});
});
