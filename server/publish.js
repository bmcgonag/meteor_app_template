import { SysConfig } from '../imports/api/systemConfig.js';
import { UserConfigOptions } from '../imports/api/userConfigOptions.js';

Meteor.publish("SystemConfig", function() {
    try {
        return SysConfig.find({});
    } catch (error) {
        console.log("    ERROR pulling system config data: " + error);
    }
});

Meteor.publish('userList', function() { 
    return Meteor.users.find({});
});
